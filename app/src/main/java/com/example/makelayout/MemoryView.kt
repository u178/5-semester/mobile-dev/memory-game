package com.example.makelayout

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.widget.ImageView

class MemoryView: androidx.appcompat.widget.AppCompatImageView {

    var mvForeground: Int? = null
    var mvBackground: Int? = null
    var isTop: Boolean = false
    var turnable: Boolean = true

    constructor(ctx: Context) : super(ctx)
    constructor(ctx: Context, attrs: AttributeSet) : super(ctx, attrs)
    fun setForeground(mvForeground_: Int) {
        this.mvForeground = mvForeground_
        isTop = true
        this.setImageResource(mvForeground!!)
    }


    fun setBackground(mvBackground_: Int) {
        this.mvBackground = mvBackground_
    }

    fun turnCard() {
        if (turnable && this.mvBackground != null && this.mvForeground != null) {
            if (isTop) {
                this.setImageResource(mvBackground!!)
            } else {
                this.setImageResource(mvForeground!!)
            }
            isTop = !isTop
        }
    }

}