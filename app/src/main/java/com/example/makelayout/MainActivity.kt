package com.example.makelayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity() {

    private var victoryFlag = false
    private var canOpenFlag = true
    private var counter = 0
    private val images = arrayOf(
        R.drawable.one_card,
        R.drawable.two_card,
        R.drawable.three_card,
        R.drawable.four_card,
        R.drawable.five_card,
        R.drawable.six_card,
        R.drawable.seven_card,
        R.drawable.eight_card
    )
    var openedImages = mutableListOf<MemoryView>()

    private suspend fun openCard(mv: MemoryView) {
        if (! mv.turnable && !canOpenFlag)
            return
        mv.turnCard()
        delay(100)
        mv.turnable = false
        openedImages.add(mv)

         if (openedImages.size == 2) {
            canOpenFlag = false
            checkCoincidence()
            canOpenFlag = true
        }

        Log.d("memorina", mv.mvForeground.toString())
    }



    private suspend fun checkCoincidence() {
        Log.d("memorina", "checkCoin")
        Log.d("memorina", "size: ${openedImages.size.toString()}")

        if (openedImages[0].mvForeground == openedImages[1].mvForeground) {
            Log.d("memorina", "Coin")
            counter += 1
            openedImages.clear()
            if (counter == 8) {
                val toast = Toast.makeText(this, "Victory!", Toast.LENGTH_LONG)
                toast.show()
            }
        } else {
                openedImages[0].turnable = true
                openedImages[1].turnable = true
                delay(800)

                openedImages[0].turnCard()
                openedImages[1].turnCard()
                openedImages.clear()
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        val layout = LinearLayout(applicationContext)
        layout.orientation = LinearLayout.VERTICAL

        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.MATCH_PARENT)
        params.weight = 1.toFloat()


        val catViews = mutableListOf<MemoryView>()
        for (i in 0..7) {
            for (j in 0..1) {
                catViews.add( // catView constructor
                    MemoryView(applicationContext).apply {
                        setForeground(images[i])
                        setBackground(R.drawable.shirt_card)
                        turnCard()
                        layoutParams = params
                        setOnClickListener{
                            GlobalScope.launch (Dispatchers.Main){
                                openCard(this@apply)
                            }
                        }
                    }
                )
            }
        }

        catViews.shuffle()


        val rows = ArrayList<LinearLayout>()
        for (i in 0..3) {
            rows.add(LinearLayout(applicationContext).apply{
                orientation = LinearLayout.HORIZONTAL
                layoutParams = params
                for (j in 0..3) addView(catViews[i * 4 + j])
            })
        }
        for (row in rows) layout.addView(row)

        setContentView(layout)
    }
}