# Simple memory game for android

5th semester Mobile Dev course 

Flip two identical cards to open them.

You win if all cards are open

### Examples

![start.png](./raw/start.png)

![progress.png](./raw/progress.png)

![victory.png](./raw/victory.png)
